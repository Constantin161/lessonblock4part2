<?php

include ('config.php');
include ('function.php');

session_start();

$pdo = new PDO(DB, LOGIN, PW);

// АВТОРИЗАЦИЯ И РЕГИСТРАЦИЯ
$status = isset($_POST['sing_in']) ? "sing_in" : (isset($_POST['registr']) ? "registr" : null);
$message = "Введите данные для регистрации или войдите, если уже зарегистрировались:";

// Обработка введенных пользователем полей
$login = isset($_POST['login']) ? $_POST['login'] : null;
$login = clearStr($login);
$pw = isset($_POST['pw']) ? $_POST['pw'] : null;
$pw = !empty($pw) ? md5(clearStr($pw)."brovkot") : null;


if(!empty($status) && (empty($login) || empty($pw))) {
    $message = "Заполните все строки!";
    }
elseif(!empty($status)) {
    $user = getUserData($pdo, $login);
    $_SESSION['is_auth'] = 1;

    if($status === 'sing_in') {
        if (empty($user) || ($user['password'] !== $pw)) {
            $message = "Вы ввели неверный логин или пароль! Попробуйте ещё раз!";
            unset($_SESSION['is_auth']);
        }
    }
    elseif($status === 'registr') {
        if (!empty($user)) {
            $message = "Пользователь с таким логином уже существует!";
            unset($_SESSION['is_auth']);
        }
        else {
            $sql = "INSERT INTO user (login, password) 
                    VALUES (?, ?)";
            $authparam = array($login, $pw);
            doneRequest($pdo, $sql, $authparam);
            $user = getUserData($pdo, $login);
        }
    }
    if(!empty($_SESSION['is_auth'])) {    
        $_SESSION['login'] = $user['login'];
        $_SESSION['id'] = $user['id'];
    }
}

if (isset($_POST['exit'])) {
    session_unset();
}

if(!isset($_SESSION['is_auth']) || empty($_SESSION['is_auth']) ) {
    include ('auth.html');
    die();
}

$user = isset($_SESSION['login']) ? $_SESSION['login'] : null;
$user_id = isset($_SESSION['id']) ? $_SESSION['id'] : null;




// РАБОТА С ЗАПИСЯМИ В БАЗЕ

$sql = "SELECT id, login FROM user WHERE id != {$user_id}";
$users = doneRequest($pdo, $sql);

$id = !empty($_POST['id']) ? $_POST['id'] : null;
$param = !empty($id) ? array($id) : null;
$action = !empty($_POST['action']) && !empty($id) ? $_POST['action'] : null;

// если была нажата кнопка "Удалить"
if($action == "delete") {
    $sql = "DELETE FROM task WHERE id = ?";
}
// если была нажата кнопка "Выполнить"
if($action == "done") {
    $sql = "UPDATE task SET is_done = 1 WHERE id = ?";
}
// если была нажата кнопка "Изменить"
if($action == "edit") {
    $sql = "SELECT description FROM task WHERE id = ?";
    }
// если была нажата кнопка "Переложить ответственность"
if($action == "change_assigned") {
    $sql = "UPDATE task SET assigned_user_id = ? WHERE id = ?";
    array_unshift($param, $_POST['assigned_user_id']);
}

// если введено что либо в строку "Описание задачи"
if(!empty($_POST['description'])) {
    $description = clearStr($_POST['description']);
    if(!empty($id)) {
        $sql = "UPDATE task SET description = ? WHERE id = ?";
        array_unshift($param, $description);
    }
    else {
        $date = date('Y-m-d H:s');
        $sql = "INSERT INTO task (description, date_added, user_id) 
                VALUES (?, ?, ?)";
        $param = array($description, $date, $user_id);
    }
}

if (!empty($param) && isset($sql)) {
    $description = doneRequest($pdo, $sql, $param);
}


$sql_arr[] = "SELECT t.id, t.description, t.is_done, t.date_added, t.user_id, t.assigned_user_id
        FROM task t
        JOIN user u ON u.id =";
$sql_arr[] = "t.user_id";
$sql_arr[] = "WHERE u.id = ?";

$sql = implode(" ", $sql_arr);

$param = array($user_id);

// если выбрана сортировка
if (isset($_POST['sorttype']) && !empty($_POST['sorttype'])) {
    $sort = $_POST['sorttype'];
    $sql .= " ORDER BY {$sort}";
}

$tasks = doneRequest($pdo, $sql, $param);

/*$sql = "SELECT t.id, t.description, t.is_done, t.date_added, t.user_id, t.assigned_user_id
        FROM task t
        JOIN user u ON u.id = t.assigned_user_id
        WHERE u.id = ?";*/

$sql_arr[1] = "t.assigned_user_id";

$sql = implode(" ", $sql_arr);

$param = array($user_id);
$tasksToDo = doneRequest($pdo, $sql, $param);

include ('form.html');
